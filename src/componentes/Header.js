import React, { useState, useEffect, useRef } from 'react';
import Pubsub from 'pubsub-js';


export default function Header() {

  const [loginPesquisado, setLoginPesquisa] = useState('');

  function pesquisa(event) {
    event.preventDefault();

    if (loginPesquisado.toString().trim() === "" || loginPesquisado === undefined) {
      return;
    }


    fetch(`https://instalura-api.herokuapp.com/api/public/fotos/${loginPesquisado.trim()}`)
      .then(response => response.json())
      .then(fotos => {
        return Pubsub.publish('pesquisaLogin', { fotos });
      });

     setLoginPesquisa("");
  }

  return (
    <header className="header container">
      <h1 className="header-logo">
        Instalura
          </h1>

      <form className="header-busca" onSubmit={pesquisa}>
        <input type="text" name="search" placeholder="Pesquisa" className="header-busca-campo" value={loginPesquisado} onChange={event => setLoginPesquisa(event.target.value)}/>
        <input type="submit" value="Buscar" className="header-busca-submit" />
      </form>


      <nav>
        <ul className="header-nav">
          <li className="header-nav-item">
            <a href="#">
              ♡
                  {/*                 ♥ */}
              {/* Quem deu like nas minhas fotos */}
            </a>
          </li>
        </ul>
      </nav>
    </header>
  );
}