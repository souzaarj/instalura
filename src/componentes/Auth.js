export const isAuthenticated = () => {

    let authenticated = true;

    if(localStorage.getItem('auth-token') === null)
        authenticated = false; 

    return authenticated;
}