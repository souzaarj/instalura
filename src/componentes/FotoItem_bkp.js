import React, { useState, Component } from 'react';
import { Link } from 'react-router-dom';
import PubSub from 'pubsub-js';

function FotoAtualizacoes(props) {

  const [likeada, setLikeada] = useState(props.foto.likeada);
  const [comentario, setComentario] = useState('');


  function like(event) {
    event.preventDefault();


    fetch(`https://instalura-api.herokuapp.com/api/fotos/${props.foto.id}/like?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, { method: 'POST' })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("não foi possível realizar o like da foto");
        }
      })
      .then(liker => {
        setLikeada(!likeada)
        PubSub.publish('atualiza-liker', { fotoId: props.foto.id, liker });
      });
  }


  function comenta(event) {

    event.preventDefault();
    const requestInfo = {
      method: 'POST',
      body: JSON.stringify({ texto: comentario }),
      headers: new Headers({
        'Content-type': 'application/json'
      })
    };


    fetch(`https://instalura-api.herokuapp.com/api/fotos/${props.foto.id}/comment?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, requestInfo)
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("não foi possível comentar a foto");
        }
      })
      .then(comentario => {
        PubSub.publish('novos-comentarios', { fotoId: props.foto.id, comentario });
      });
      
    setComentario('');
  }


  return (
    <section className="fotoAtualizacoes">
      <a onClick={like} className={likeada ? 'fotoAtualizacoes-like-ativo' : 'fotoAtualizacoes-like'}>Linkar</a>
      <form className="fotoAtualizacoes-form" onSubmit={comenta}>
        <input type="text" placeholder="Adicione um comentário..." className="fotoAtualizacoes-form-campo" value={comentario} onChange={(event) => { setComentario(event.target.value) }} />
        <input type="submit" value="Comentar!" className="fotoAtualizacoes-form-submit" />
      </form>

    </section>
  );
}

class FotoInfo extends Component {

  constructor(props) {
    super(props);
    this.state = {
      likers: this.props.foto.likers,
      comentarios: this.props.foto.comentarios
    }

  }

  componentWillMount() {


    PubSub.subscribe('atualiza-liker', (topico, infoLiker) => {

      let novosLikers = null;

      if (this.props.foto.id === infoLiker.fotoId) {
        const possivelLiker = this.state.likers.find(liker => liker.login === infoLiker.liker.login);
        if (possivelLiker === undefined) {
          novosLikers = this.state.likers.concat(infoLiker.liker);
        } else {
          novosLikers = this.state.likers.filter(liker => liker.login != infoLiker.liker.login);
        }
        this.setState({ likers: novosLikers });
      }
    })


    PubSub.subscribe('novos-comentarios', (topico, infoComentario) => {

      if (this.props.foto.id === infoComentario.fotoId) {
        const novosComentarios = this.state.comentarios.concat(infoComentario.comentario);
        this.setState({ comentarios: novosComentarios });
      }
    })
  }

  render() {
    return (

      <div className="foto-info">
        <div className="foto-info-likes">
          {
            this.state.likers.map(liker => {
              return <Link key={liker.login} to={`/app/${liker.login}`}>{liker.login},</Link>
            })
          }
          curtiram
        </div>

        <p className="foto-info-legenda">
          <a className="foto-info-autor">autor </a>
          {this.props.foto.comentario}
        </p>

        <ul className="foto-info-comentarios">

          {this.state.comentarios.map(comentario => {
            return (
              <li className="comentario" key={comentario.id}>
                <Link to={`/app/${comentario.login}`} className="foto-info-autor">{comentario.login} </Link>
                {comentario.texto}
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

function FotoHeader(props) {
  return (
    <header className="foto-header">
      <figure className="foto-usuario">
        <img src={props.foto.urlPerfil} alt="foto do usuario" />
        <figcaption className="foto-usuario">
          <Link to={`/app/${props.foto.loginUsuario}`}>
            {props.foto.loginUsuario}
          </Link>
        </figcaption>
      </figure>
      <time className="foto-data">{props.foto.horario}</time>
    </header>
  );
}

export default function FotoItem(props) {
  return (
    <div className="foto">
      <FotoHeader foto={props.foto} />
      <img alt="foto" className="foto-src" src={props.foto.urlFoto} />
      <FotoInfo foto={props.foto} />
      <FotoAtualizacoes foto={props.foto} />
    </div>
  );
}