import React, { Component } from 'react';
import FotoItem from './FotoItem';

export default class Timeline_bkp extends Component {

  constructor(props) {
    super(props);
    this.state = { login: {}, fotos: [] };
    this.login = this.props.login;
  }

  carregaFotos() {

    let urlPerfil;

    if (this.login === undefined) {
      urlPerfil = `https://instalura-api.herokuapp.com/api/fotos?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`;
    } else {
      urlPerfil = `https://instalura-api.herokuapp.com/api/public/fotos/${this.props.login}`;
    }

    fetch(urlPerfil)
      .then(res => {
        if (!res.ok) throw Error(res.statusText);
        return res
      })
      .then(res => res.json())
      .then(fotos => {
       
         return this.setState({ fotos: fotos })
       
      })
      .catch(erro => console.error(erro))
  }

  componentDidMount() {
    this.carregaFotos();
  }

  static getDerivedStateFromProps(nextProps, prevState) {

    if (nextProps.login !== prevState.login) {
    console.log(nextProps)
               
      return { login: nextProps.login, 
               foto: nextProps.fotos};
    }

    return null;

    /* if (nextProps.login !== undefined) {
      this.login = nextProps.login;
      this.carregaFotos();
    }*/

  }


  componentDidUpdate() {
    this.carregaFotos();
  }

  render() {

    return (
      <div className="fotos container">
        {
          this.state.fotos.map((foto, index) => <FotoItem key={foto.id} foto={foto} />)
        }
      </div>
    );
  }
}