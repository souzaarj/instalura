import React from "react";
import Header from './Header';
import Timeline from './Timeline';

//import { Redirect } from "react-router-dom";
//import { isAuthenticated } from './Auth';

export default function App(props) {


  return (
    <div className="App">
      <Header />
      <Timeline login={props.match.params.login} />
    </div>
  );
}
