import React, { useState, useEffect } from 'react';

export default function TesteHooks() {
    // Declara uma nova variável de state, que chamaremos de "count"
    const [count, setCount] = useState(0);

    // Similar ao componentDidMount e componentDidUpdate:
    useEffect(() => {
        // Atualiza o titulo do documento usando a API do browser
        document.title = `Você clicou ${count} vezes`;
    console.log('teste');
        teste();

        
    });

    function teste(){
        console.log('entrei no teste'); 

        return ()=>{'destrui o teste'};
    }


    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={() =>setCount(count + 1)}>
                Click me
        </button>
        </div>
    );
}