import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';


function FotoAtualizacoes(props) {

  const [likeada, setLikeada] = useState(props.foto.likeada);
  const [comentario, setComentario] = useState('');


  function like(event) {
    event.preventDefault();

    setLikeada(!likeada)
    props.like(props.foto.id);

  }

  function comenta(event) {

    event.preventDefault();

    props.comenta(props.foto.id, comentario);

    setComentario('');
  }


  return (
    <section className="fotoAtualizacoes">
      <a onClick={like} className={likeada ? 'fotoAtualizacoes-like-ativo' : 'fotoAtualizacoes-like'}>Linkar</a>
      <form className="fotoAtualizacoes-form" onSubmit={comenta}>
        <input type="text" placeholder="Adicione um comentário..." className="fotoAtualizacoes-form-campo" value={comentario} onChange={(event) => { setComentario(event.target.value) }} />
        <input type="submit" value="Comentar" className="fotoAtualizacoes-form-submit" />
      </form>

    </section>
  );
}

function FotoInfo(props) {


  const [likers, setLikers] = useState(props.foto.likers);
  const [comentarios, setComentarios] = useState(props.foto.comentarios);

  return (

    <div className="foto-info">
      <div className="foto-info-likes">
        {
          likers.map(liker => {
            return <Link key={liker.login} to={`/app/${liker.login}`}>{liker.login},</Link>
          })
        }
        curtiram
        </div>

      <p className="foto-info-legenda">
        <a className="foto-info-autor">autor </a>
        {props.foto.comentario}
      </p>

      <ul className="foto-info-comentarios">

        {comentarios.map(comentario => {
       
        return (
           
            <li className="comentario" key={comentario.id}>
              <Link to={`/app/${comentario.login}`} className="foto-info-autor">{comentario.login} </Link>
              {comentario.texto}
            </li>
          )
        })}
      </ul>
    </div>
  );
}

function FotoHeader(props) {
  return (
    <header className="foto-header">
      <figure className="foto-usuario">
        <img src={props.foto.urlPerfil} alt="foto do usuario" />
        <figcaption className="foto-usuario">
          <Link to={`/app/${props.foto.loginUsuario}`}>
            {props.foto.loginUsuario}
          </Link>
        </figcaption>
      </figure>
      <time className="foto-data">{props.foto.horario}</time>
    </header>
  );
}

export default function FotoItem(props) {
  return (
    <div className="foto">
      <FotoHeader foto={props.foto} />
      <img alt="foto" className="foto-src" src={props.foto.urlFoto} />
      <FotoInfo foto={props.foto}/>
      <FotoAtualizacoes {...props}/>
    </div>
  );
}