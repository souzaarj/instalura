import React, { useState, useEffect } from 'react';
import FotoItem from './FotoItem';
import Pubsub from 'pubsub-js';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

export default function Timeline(props) {

    const [login] = useState(props.login);
    const [fotos, setFotos] = useState([]);

    useEffect(() => {
          carregaFotos();
    }, [login])


    useEffect(() => {


        Pubsub.subscribe('timeline',(topico,fotos) => {
            setFotos({fotos});
          });
        

        Pubsub.subscribe('pesquisaLogin', (topico, loginConsultado) => {
            setFotos(loginConsultado.fotos);
        })

        Pubsub.subscribe('atualiza-liker', (topico, infoLiker) => {

            let fotoAchada = fotos.find(foto => foto.id === infoLiker.fotoId)
            
            if (fotoAchada) {

                fotoAchada.likeada = !fotoAchada.likeada;

                const possivelLiker = fotoAchada.likers.find(liker => liker.login === infoLiker.liker.login);

                if (possivelLiker === undefined) {
                    fotoAchada.likers.push(infoLiker.liker);

                } else {
                    const novosLikers = fotoAchada.likers.filter(liker => liker.login !== infoLiker.liker.login);
                    fotoAchada.likers = novosLikers;
                }

                setFotos(fotos);

            }
        })



        /*  Pubsub.subscribe('novos-comentarios', (topico, infoComentario) => {
 
             if (props.foto.id === infoComentario.fotoId) {
                 console.log('comenarios', comentarios)
                 const novosComentarios = comentarios.concat(infoComentario.novoComentario);
 
                 setComentarios(novosComentarios);
 
             }
         })
  */

    })




    function like(fotoId) {

        fetch(`https://instalura-api.herokuapp.com/api/fotos/${fotoId}/like?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, { method: 'POST' })
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error("não foi possível realizar o like da foto");
                }
            })
            .then(liker => {
                Pubsub.publish('atualiza-liker', { fotoId, liker });
            });
    }

    function comenta(fotoId, textoComentario) {

        const requestInfo = {
            method: 'POST',
            body: JSON.stringify({ texto: textoComentario }),
            headers: new Headers({
                'Content-type': 'application/json'
            })
        };


        fetch(`https://instalura-api.herokuapp.com/api/fotos/${fotoId}/comment?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`, requestInfo)
            .then(response => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error("não foi possível comentar a foto");
                }
            })
            .then(novoComentario => {
                Pubsub.publish('novos-comentarios', { fotoId, novoComentario });
            });
    }


    return (

        <div className="fotos container">
            <ReactCSSTransitionGroup
                transitionName="timeline"
                transitionEnterTimeout={500}
                transitionLeaveTimeout={300}>
                {
                    fotos.map((foto, index) => <FotoItem key={foto.id} foto={foto} like={like} comenta={comenta} />)
                }
            </ReactCSSTransitionGroup>

        </div>

    )


    function carregaFotos() {

        let urlPerfil;

        if (login === undefined) {
            console.log('carreguei com token');
            urlPerfil = `https://instalura-api.herokuapp.com/api/fotos?X-AUTH-TOKEN=${localStorage.getItem('auth-token')}`;
        } else {
            console.log('carreguei sem token');
            urlPerfil = `https://instalura-api.herokuapp.com/api/public/fotos/${login}`;
        }

        fetch(urlPerfil)
            .then(res => {
                if (!res.ok) throw Error(res.statusText);
                return res
            })
            .then(res => res.json())
            .then(fotosRecebidas => setFotos(fotosRecebidas))
            .catch(erro => console.error(erro))

    }


}