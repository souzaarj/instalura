import React, { useState } from 'react';
import { withRouter } from 'react-router-dom'

export default function Login(props) {

    const [msg, setMsg] = useState('');
    //const { state = {} } = this.props.location;
    const [login, setLogin] = useState('');
    const [senha, setSenha] = useState('');


    function envia(event) {
        event.preventDefault();

        const requestInfo = {
            method: 'POST',
            body: JSON.stringify({ login: login, senha: senha }),
            headers: new Headers({
                'Content-type': 'application/json'
            })
        }

        fetch('https://instalura-api.herokuapp.com/api/public/login', requestInfo)
            .then(response => {
                if (response.ok) {
                    return response.text();
                } else {
                    throw new Error('Não foi possível efetuar o login.');
                }

            })
            .then(token => {
                localStorage.setItem('auth-token', token);
                withRouter(props.history.push("/app"))
                console.log(token);
            })
            .catch(erro => {
                setMsg(erro.message);
            })
    }

    return (<div className="login-box">
        <h1 className="header-logo">Instalura</h1>
        <span>{msg}</span>
        <form onSubmit={envia}>
            <input type="text" value={login} onChange={event=>setLogin(event.target.value)} />
            <input type="password" value={senha} onChange={event=>setSenha(event.target.value)} />
            <input type="submit" value="login" />
        </form>
    </div>);


}