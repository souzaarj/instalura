import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Login from './Login';
import { isAuthenticated } from './Auth';
import App from './App';
import Logout from './Logout';
import TesteHooks from './TesteHooks';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        isAuthenticated()
            ? (<Component {...props} />)
            : (<Redirect to={{ pathname: '/', state: { msg: '', from: props.location } }} />)
    )}
    />
)
const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/app/:login" component={App} />
            <Route path="/testehooks" component={TesteHooks} />
            <PrivateRoute path="/app" component={App} />
            <PrivateRoute path="/logout" component={Logout} />
            <Redirect from="/*" to="/app" />
        </Switch>
    </BrowserRouter>
)
export default Routes; 