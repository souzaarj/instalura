import { useEffect } from 'react';
import { withRouter } from "react-router-dom";

function Logout(props) {

    useEffect( () => {
        localStorage.removeItem('auth-token');
        props.history.push('/');
    },[])

        return null;
}

export default withRouter(Logout)